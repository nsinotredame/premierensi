# Complexité

activité d'introduction à la complexité : [activité](complexite.html)

# Tri par sélection 

Activité sur le tri par sélection : [activité](tri_selection.html)

La page wikipédia est très intéressante : [Page wikipedia sur le tri selection](https://fr.wikipedia.org/wiki/Tri_par_s%C3%A9lection)

Cours sur le tri par sélection : [cours](cours_tris_selection.pdf)

# Tri par insertion

Activité sur le tri par insertion : [activité](tri_insertion.ipynb)

Cours sur le tri par insertion : [cours](cours_tris_insertion.pdf)

Exercices sur les tris : [exercices](exercices_tris.pdf)

# Recherche dichotomique

!!! example "Activité sur le jeu du devine le nombre"

    ```python
    def devineur(borne_inf,borne_sup):
    gagne = False
    nombre = (borne_sup - borne_inf) // 2

    print("Pensez à un nombre entre " + borne_sup + " et " + borne_inf)
    reponse = input("Est-ce" + nombre + "? (oui/non)")
    if reponse == "oui":
        gagne = True

    while not(gagne):
        reponse = input("Est-ce + ou - que " + nombre + "?")
        if reponse == "+":
            #faire quelque chose
        else:
            #faire quelque chose

        reponse = input("Est-ce" + nombre + "? (oui/non)")
        if reponse == "oui":
            gagne = True
    ```