# Architectures des machines

## Un peu d'histoire

!!! tip "C'est pas sorcier : NANOMONDE SE SECOUE LES PUCES"
    Histoire des ordinateurs et fonctionnement du transistor: jusqu'à 13 minutes environs. Le reste parle de miniaturisation, que vous verrez en terminale.

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oq6Ol550K3w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Il est difficile de savoir quel a été le premier ordinateur, quand on remonte dans l'histoire, on voit différentes machines réalisées toujours dans le même but : effectuer des calculs longs et fastidieux. On considère les premiers vrais ordinateurs comme étants les premières machines totalement numériques et non analogiques

!!! faq "Différence entre numérique et analogique?" 
    Sur une machine analogique, les entrées et les sorties correspondent à des grandeurs physiques concrètes, tel que l'électricité (la variation d'une tension par exemple).
    Tandis qu'une machine numérique va gérer des entrées et des sorties encodées, en binaire par exemple.
    
    Par exemple : 
    
    - une cassette audio est analogique car le son capté par une membrane, a fait vibrer un aimant qui a magnétisé une bande magnétique, la variation de la magnétisation de la bande représente le son et peut être lu avec le procédé inverse

    - un cd est numérique, le son a été traduit sur un ordinateur pour qu'il corresponde à une séquence de 0 et de 1, un laser va graver les 0 et les 1 sur le disque, le son est stocké de manière virtuelle sur le disque, les 0 et 1 ne correspondent à rien en soit il faut le traduire en son

### Première machines à calculer

L'objectif des machines à calculer était, comme leur nom l'indique de calculer, on était bien loin de l'ordinateur personnel d'aujourd'hui.

![](20230323092257.png){: .center width=30%}

*La pascaline, machine à calculer du XVIIe siècle*

### Passage à l'électronique

Les tubes à vide sont des composants électroniques permettant de faire passer ou non le courant dans un circuit. Pas très différent d'un interrupteur me direz vous? La différence c'est qu'on peut activer ou non le courant en fonction d'un autre courant! Le monde évolue progressivement des machines mécaniques aux machines électroniques.

![](20230323093711.png){: .center width=30%}

*Voici l'ENIAC en 1945, un des tous premiers ordinateurs fonctionnant avec des tubes à vide*

### Invention du transistor

Les tubes à vide présentaient de gros problemes, cela générait beaucoup de chaleur, consommait beaucoup d'électricité, et provoquait énormément de pannes en raison de leur fragilité. En 1947 le transistor est invgenté.

Le transistor est un composant électronique qui comme le tube à vide, permet de laisser passer ou non le courant en fonction d'un autre courant en entrée. L'avantage est qu'il est petit, et sujet à moins de pannes que les tubes à vide. L'invention du transistor a conduit à la course à la miniaturisation.

![](20230323095116.png){: .center width=30%}

*L'Intel 4004, premier microprocesseur sorti en 1971, il contient 2300 transistors, pour comparaison les derniers processeurs de nos jours en contiennent plusieurs milliards*

## Circuits combinatoires

Grâce aux semi-conducteurs, on peut créer ce que l'on appelle des portes logiques. Une porte logique est une implémentation en éléctronique d'une fonction booléenne.

Dans la suite du cours nous utiliserons 0 et 1 comme valeurs booléennes et non True/False ou Vrai/Faux

### Porte NON

!!! abstract "Porte NON"
    ![](20230323084018.png){: .center width=30%}

    Par exemple la porte logique la plus simple est l'implémentation de la fonction booléenne **NON**.
    Pour rappel, la table de vérité: 

    |A|NON(A)|
    |-------|-------|
    |0|1|
    |1|0|

    Donc une porte logique NON se comportera ainsi : la sortie est activée uniquement si aucun courant ne passe en entrée.

### Porte OU

!!! abstract "Porte OU"
    ![](20230323091844.png){: .center width=30%}

    Implémentation de la fonction OU, dont la table de vérité est la suivante:

    |A|B|A OU B|
    |-|-|------|
    |0|0|0|
    |0|1|1|
    |1|0|1|
    |1|1|1|

    La sortie est activée si le courant passe dans au moins une des deux entrées

### Porte ET

!!! abstract "Porte ET"
    ![](20230323091659.png){: .center width=30%}

    Implémentation de la fonction ET, dont la table de vérité est la suivante:

    |A|B|A OU B|
    |-|-|------|
    |0|0|0|
    |0|1|0|
    |1|0|0|
    |1|1|1|

    La sortie est activée si le courant passe dans les deux entrées uniquement

### Exercice 1

!!! example "Exercice 1"

    - 1 Identifier la porte logique [suivante](https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IHMCGAXApgZygbVCgngA5pQgCCAcgCIjyED2Wk2A7CwAywDMHAuvAEsAdjk4BGfiHoBXFFABM8DAAt6AdzIYAqkIDWQ9SMgoATtLQBfScJygGTVu05cAbO2sATKF3hgkAGyh2C1g7RhwOZwBOd0EvGF8AoKt4GTlmMIcADmhnPjioAFYUkDUBE0wcbB9Y7DgJWGxFQt5eCyA)

    - 2 Identifier la porte logique [suivante](https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7Azmg7AAyGwDMAbIQLrwIAmUp8YAhgDZSEC+smuBxMgE5qtBjGbtOXGiCwBXAC5o+eSKgAc0EqSKz6UAKwz4AcxaKApmvQhFATwyWoIAPIAlEPGw38RMnq0KOokAIyyCsqQAEzwOAAWWADuAII4AKpIANZIycGKAE7yliYgSQgF1mioTKKocOGwqLGGVFRcQA)

    - 3 Identifier la porte logique [suivante](https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7Azmg7AAyGwDMAbIQLrwIAmUp8YAhgDZSEC+smuBxMgE5qtBjGbtOXGiCwBXAC5o+eSKgAc0EqSKz6UAKwz4AcxaKApmvQhFATwyWoIAHIBBVwBEQ8bDfwiMj1aFHUSAEZZBWVIACZ4HAALLAB3dxwAVSQAayQ0sMUAJ3lLExBUhCLrNFQmUVQ4KNhUBMMqKi4gA)
    
### Implémenter d'autres fonctions logique

On a vu que toutes les portes logiques peuvent être constituées de portes **NON**, **ET** et **OU** uniquement,  dans la suite du cours nous allons réaliser des circuits un peu plus complexes.

Comment réaliser un circuit combinatoire à partir d'une fonction logique?

- 1 On écrit la table de vérité de la fonction
- 2 On regarde les valeurs des entrées qui donnent 1 en sortie
- 3 On rejoint par des portes **OU** les combinaisons qui donnent 1 en sortie, et par des portes **ET** les variables de chaque combinaison.

!!! faq "Exemple avec la porte XOR/OU Exclusif"
    Voilà la table de vérité de la fonction OU Exclusif, aussi appelée XOR:

    |A|B|A XOR B|
    |-|-|-------|
    |0|0|0|
    |0|1|1|
    |1|0|1|
    |1|1|0|

    On voit que les combinaisons qui donnent 1 en sortie sont:

    - A=0, B=1
    - B=1, A=0

    On rejoint par un **OU** les deux combinaisons, ce qui nous donne:

    A XOR B = (NON A ET B) OU (A ET NON B)

    On peut ensuite réaliser ce circuit avec des portes logiques: [comme dans cet exemple](https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFRVopMgA5liVRULH4AczYWuW3QQCwBPDC1+ADkAEX1sIOcmJ2STZDQ4fBNzK0gANilQMIjePlj42Tsk0kU3dLtCWGdsyygATkKQ8MiAeQAlCqC2+Rc6lDsqWAoKFtyKJk7iyKiegBVBtEJHcio08Yp1U1bICmhF7t4V9fgEze3oRz3xeByoCm8QAHcEACdAtHscGgtFQ6iysFQLAOILyU2UqDaUxmEPmU0IIIoTRB6nhLHR1AkQA)

!!! faq "Produit de sommes ou somme de produits?"
    Dans l'électronique, on appelle par abus de langage les **ET comme des produits** et les **OU comme des sommes**. Si on voulait être exact on parlerait de **conjonction** pour les ET et de **disjonction** pour les OU.

    Pour réaliser un circuit logique avec uniquement des portes ET, OU, NON, on doit exprimer la fonction logique:
    
    - Comme une somme de produits, si la fonction logique a peu de "1" dans la table de vérité.
    
    Exemple de somme de produits : **(A ET NON(B)) OU (NON(B) ET NON(A)) OU (A ET B)**


    - Comme un produit de sommes, si la fonction logique a peu de "0" dans la table de vérité.
    
    Exemple de somme de produits : **(A OU NON(B)) ET (NON(B) OU NON(A)) ET (A ET B)**

### Exercice 2

!!! example "Exercice 2"

    On souhaite implémenter la fonction logique de l'implication, voici sa table de vérité:

    |A|B|A => B|
    |-|-|------|
    |0|0|1|
    |0|1|1|
    |1|0|0|
    |1|1|1|

    Voici le circuit correspondant à cette table: [lien vers le circuit](https://logic.modulo-info.ch/?mode=tryout&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQUmTUmRImGUCoWPwA5mwtctuggFgCeGFr8AHIAIvrYQQAcTuRObih2cPgm5laQAGxSoGERvHyx8bJ2yaTQxMqIGaiEsIk5llAAnEUh4ZHlcfAJaJ1K5IkNyGhUsBQU7XkOQ7hcfLYgAPIASiA9JZFRGwAqlUGEdbBMnZMZFOqmHZAU0Ht9vNun08T4ta50TRR8LN8gsPIlXqVtANPtUUtBrulpp1Lg1cswKBCDscYc0LmQEf9mCwHnkmGRMbxDidlmc8UpEZAmHAScx8N4QAB3BAAJ0CaHscGgtFQLDuwvys2UqGRc2FS0BwrJsEIwvUbVgIsuTEVQOyGvUTMVs06qpRioowLliVmKuoEiAA)
    
    - 1 Quelqu'un a trafiqué l'un des composant, quelle porte logique est defectueuse?

    Il est parfois plus simple de réaliser un circuit à partir d'un produit de sommes, au lieu d'une somme de produits comme nous avons vu avec l'exemple du OU, **c'est à dire en rejoignant les variables par des OU et les combinaisons par des ET, le tout en inversant la valeur des variables**

    - 2 Réaliser le circuit de l'implication avec un produit de sommes: [lien](https://logic.modulo-info.ch/?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQBOKuQBsyxKoqFjEoA)

### Exercice 3

!!! example "Exercice 3"
    Voici la table de vérité correspondant à une fonction logique:

    |A|B|C|Sortie|
    |-|-|-|------|
    |0|0|0|0|
    |0|0|1|0|
    |0|1|0|1|
    |0|1|1|1|
    |1|0|0|1|
    |1|0|1|1|
    |1|1|0|1|
    |1|1|1|1|

    - 1 Que serait le plus judicieux? Somme de produits ou produit de sommes?
    - 2 Réaliser le circuit correspondant à la table de vérité : [lien](https://logic.modulo-info.ch/?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQBOKuQBsyxKoqFjEoA)
    - 3 Simplifier la fonction pour qu'elle ne dépende que de A et de B
    - 4 Réaliser le circuit simplifié : [lien](https://logic.modulo-info.ch/?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQBOKuQBsyxKoqFjEoA)

### Circuits plus complexes 


### Exercice 4

!!! example "Exercice 4"

    === "Enoncé"
        Voici la fonction logique du multiplexeur:

        (E1 **ET** **NON** C) **OU** (C **ET** E2)

        - 1 Cette fonction logique est-elle une somme de produits ou un produit de sommes?

        - 2 Réaliser le circuit correspondant à cette fonction

        - 3 Compléter la table de vérité ci dessous : ![](20230323132447.png)

        - 4 A quoi sert le multiplexeur?
    
    === "Corrigé"
        !!! abstract "Le multiplexeur"
            Un multiplexeur (abréviation : MUX) est un circuit permettant de concentrer sur une même voie de transmission différents types de liaisons (informatique, télécopie, téléphonie, télétex) en sélectionnant une entrée parmi N. 

            Il possède une sortie et N entrées, ainsi qu’une entrée de commande de plusieurs bits permettant de choisir quelle entrée sera sélectionnée. 

            Il sert d’accès aux réseaux de transmission de données numériques ou analogiques.

### Exercice 5 

!!! example "Exercice 5"
    On considère une fonction logique à 2 **e0 et e1** entrées et 4 sorties, **s0, s1, s2, et s3**
    Pour chaque entrée **e0 et e1**, une sortie **sN** est égale à 1, tel que N est la valeur décimale codée par e0 et e1 en binaire, les autres sorties sont égales à 0.

    - 1 Compléter la table de vérité suivante:

    |e0|e1|s0|s1|s2|s3|
    |--|--|--|--|--|--|
    |0 |0 |  |  |  |  |
    |0 |1 |  |  |  |  |
    |1 |0 |  |  |  |  |
    |1 |1 |  |  |  |  |

    - 2 Exprimer s0, s1, s2, s3 en fonction de e0 et e1 par un produit de sommes ou une somme de produits

    - 3 Réaliser le circuit logique de cette fonction, que l'on appelle le démultiplexeur: [lien](https://logic.modulo-info.ch/?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQBOKuQBsyxKoqFjEoA)

### Exercice 6

!!! example "Exercice 6: demi-additionneur et additionneur 1bit"

    - 1 Effectuer les additions binaires: **0+0,0+1,1+0,1+1**
    
    Un demi-additionneur binaire 1 bit est un circuit combinatoire qui possède :

    - deux entrées : deux bits d'opérande **e0** et **e1** ;

    - deux sorties : un bit de résultat **s** et un bit de retenue sortante **r** .

    La sortie **s** prend pour valeur le bit des unités et la sortie **r** le bit de retenue sortante, lorsqu'on additionne les deux bits d'entrée **e0** et **e1** .

    - 2 Compléter la table de vérité de ce circuit combinatoire:

    |e0|e1|s|r|
    |--|--|--|--|
    |0 |0 |  |  |
    |0 |1 |  |  |
    |1 |0 |  |  |
    |1 |1 |  |  |

    - 3 Construire le circuit logique correspondant : [lien](https://logic.modulo-info.ch/?mode=design&data=N4IgbiBcAsA0IEsB2UDaoAOB7AzmgrAOwAMsAzAIzEC68CAJlAEzxICGAtgKZQgCCIeGDYAbKMQC+sTLgIlY0MjTqNIZVpx6QQAIUHhR4ibRBYArgBc0MvJFQBOKuQBsyxKoqFjEoA)

## TP : Afficheur 7 segments

!!! example "Afficheur 7 segments : TP par équipe"
    Vous allez réaliser un afficheur 7 segments : [Aidez vous de la page wikipedia](https://fr.wikipedia.org/wiki/Affichage_%C3%A0_sept_segments)

    ![](20230411100949.png)

    Le but est de réaliser un circuit qui prendra un nombre (codé en binaire bien entendu) en entrée, et qui en fonction de ce nombre, actionnera ou non ses différents segments A,B,C,D,E,F et G

    !!! warning "Consignes"
        Vous allez vous répartir en 7 groupes différents, chaque groupe s'occupera de réaliser le circuit d'un segment:
        

        - Avant de toucher à quoi que ce soit, vous réaliserez la table de vérité de votre segment.
        
        - Une fois la table de vérité écrite, vous réaliserez la fonction logique décrivant cette table de vérité à l'aide d'une **somme de produits** ou d'un **produit de sommes** en choisissant la solution comportant le moins d'opérateurs.

        - Chaque équipe doit installer sur son ordinateur le logiciel **logisim** à l'aide de la commande `sudo apt install logisim`, l'interface est un peu différente de celle vue dans le cours mais il n'y a rien de bien sorcier, il est juste un peu plus technique, vous retrouverez de l'aide plus loin.

        - Vous utiliserez [ce fichier](circuit_base.circ) pour implémenter votre fonction logique sous forme d'un circuit logique

    !!! tip "Aide logisim"

        Voilà de l'aide pour utiliser logisim: [](https://benhadid.github.io/l2ado/static_files/docs/Logisim_tutoriel)

    !!! info "Réalisations des groupes" 

        [Segment A](tp_logisim/segment_A.circ)

        [Segment B](tp_logisim/segment_B.circ)

        [Segment C](tp_logisim/segment_C.circ)

        [Segment D](tp_logisim/segment_D.circ)

        [Segment E](tp_logisim/segment_E.circ)

        [Segment F](tp_logisim/segment_F.circ)

        [Segment G](tp_logisim/circuit_segment_G.circ)