# Tp : échanges sur la couche transport

Nous nous concentrerons sur la couche Transport d'Internet, vous avez déjà vu en seconde en SNT, le protocole TCP qui assure la fiabilité des transmissions.
Nous verrons une des façons de fiabiliser une transmission sur Internet : le bit de contrôle.

!!! info "Objectif"
    L'objectif de ce TP est de réaliser une implémentation du protocole du bit alterné pour l'envoi de données d'une machine à une autre grâce au protocole UDP

On veut réaliser l'envoi d'une donnée depuis la machine A vers la machine B en fiabilisant l'envoi avec le protocole du bit alterné, selon le schéma suivant: 

![](20221110091048.png)  

L'envoi des données utiltsera le protocole UDP qui permet d'envoyer des données d'une machtne à une autre de manière unidirectionnelle et sans contrÔle de bonne réception. Pour réaliser ce contrôle de réception on va implémenter le protocole du bit alterné. Le paquet UDP est encapsulé dans un paquet IP, de la même  manière que pour TCP.
Dans ce TP,la machine A enverra les données et le bit de contrôle vers La machine B en utilisant le port 56789 de cette dernière. La machine B renverra le bit de contrôle reçu sur le port 56790 de la machine A.

On va programmer en Python deux programmes nommés `MachineA.py` et `MachineB.py` qui vont communiquer entre eux par l'intermédiaire du réseau.

## Réception de la machine B

Pour pouvoir communiquer en réseau avec Python, on utilisera le module socket:

```python
import socket
```

On crée ensuite l'élément `receveur` qui permettra de recevoir les paquets UDP avec le code suivant:

```python
receveur = socket.socket(type=socket.SOCK_DGRAM)
```

On indique ensuite depuis quelle machine et sur quel port `receveur` accepte les données avec l'instruction suivante :

```python
receveur.bind(('localhost',56789))
```

Dans un premier temps, on utilise `localhost` comme nom de machine qui est un alias permettant de toujours indiquer sa propre machine: les échanges se feront en interne de la machine qui jouera le rôle des machines A et B, mais en utilisant les mêmes fonctionnalités que pour un échange par le réseau. Enfin, on se met en attente de recevoir des données de taille au maximum 1500 octets. Lorsque les données sont reçues, on récupère la donnée et un tuple (adress,port) de la machine qui l'a envoyée.

```python
donnee, adress = receveur.recvfrom(1500)
```

!!! example "A vous"
    - Créer le programme MachineB.py en ajoutant les lignes nécessaires pour afficher la donnée reçue et l'adresse de l'émetteur. Lancer le programme. Il reste en attente tant qu'il n'a pas reçu de données.

## Emission de la machine A

!!! example "A vous"
    - Créer le programme MachineA.py qui crée l'élément `emetteur` de la même manière que pour la création de `receveur` dans MachineB.py

    Pour envoyer les données, on utilise le code:

    ```python
    emetteur.sendto(donnee, ("localhost", 56789))
    ```

    L'argument `donnee` doit être une chaine d'octets. Pour en définir une simplement, on ajoute `b` devant une chaine de caractères : `donnee = b'Hello world'`

    - Lancer le programme MachineA.py et vérifier que la donnée est bien reçue par le programme MachineB.py

## Echanges entre plusieurs machines

Fichiers : [MachineA.py](data/MachineA.py) et [MachineB.py](data/MachineB.py)

!!! example "A vous"
    - Avec la commande `ipconfig` (Windows) ou `ip address` (Linux), identifier l'adresse de votre machine.

    - Echanger cette adresse avec votre voisin et modifier chacun vos codes pour que l'un d'entre vous puisse exécuter MachineA.py envoyant des données à la machine de votre voisin qui exécute MachineB.py

## Ajout du bit de contrôle

!!! example "A vous"
    - Créer dans MachineA.py la fonction `envoiAvecBit(donnee, bit, emetteur, adresse, port)` qui remplace `emetteur.sendto()` et ajoute le bit de contrôle à ce qui est envoyé.

    - Modifier MachineB.py pour afficher en plus de la donnée et de l'adresse la valeur du bit de contrôle.

## Renvoi du bit de contrôle

Le programme MachineB.py doit maintenant renvoyer la valeur du bit reçu. 

!!! example "A vous"
    - Ajouter à MachineB.py le code permettant de faire cet envoi, et à MachineA.py le code permettant de le recevoir.

Le numéro de port utilisé est maintenant 56790

## Envois multiples

!!! example "A vous"
    - Utiliser les codes déjà écrits pour faire en sorte que la machine A envoie une suite de données à la machine B avec vérification du bit de contrôle

En cas d'erreur, la donnée est renvoyée avant de passer à la suivante.

## Simuler des pertes réseaux

!!! example "A vous"
    - Avec la fonction `randint()` du module `random`, simuler la mauvaise réception d'un paquet sur 3.