# Systèmes d'explotation et commandes linux

Le système d’exploitation est le logiciel principal d’une machine, qui permet aux applications d’utiliser les ressources matérielles de la machine : processeur, mémoire, périphériques…

L’utilisateur peut ensuite utiliser ces applications sans se soucier du fonctionnement de sa machine

```mermaid
graph TD;
A(Utilisateur)-->B(Application);
B-->C(Système d'exploitation);
C-->D(Ressources matérielles);
D-->C
C-->B
B-->A
```

!!! info "Logiciel libre ou propriétaire?"
    Il existe deux types de systèmes d’exploitation : les systèmes libres et propriétaires.

    Un système d’exploitation ou logiciel libre est un logiciel dont le partage et la modification sont permis, et dont le code source est en libre accès

    Un système d’exploitation ou logiciel propriétaire est protégé par le droit d’auteur et est généralement payant (mais il se peut que des logiciels propriétaires soient gratuits!)


!!! faq "[Vidéo à regarder](https://www.youtube.com/watch?v=4OhUDAtmAUo)"

    === "Lister les différentes fonction du système d'exploitation"

        - ...
        - ...
        - ...
        - ...

    === "Corrigé"

        - Gestion des fichiers
        - Gestion de la mémoire
        - Gestion des applications
        - Gestion des entrées/sorties


## Chronologie des systèmes d'exploitation:

```mermaid
graph LR;
A(Unix - 1969)-->B(Linux - 1991);
B-->C(Debian - 1996)
B-->D(Android - 2008)
A-->E(Mac OS - 1984)
E-->F(Mac OS X/macOS - 2000)
F-->G(iPhoneOS/iOS - 2007)
H(MS-DOS - 1981)-->I(Windows 1.0 - 1985)
I-->J(Windows NT - 1993)
```

!!! example "A vous de chercher!"
    === "Recherche"
        Rechercher sur internet les systèmes d'exploitation sur le schéma précédent et dire si ils sont libres ou propriétaires

        - Unix : 

        - Linux : 

        - Debian : 

        - Android :

        - Mac OS/Mac OS X:

        - iPhone/iOS :

        - MS-DOS :

        - Windows :
    
    === "Corrigé"

        - Unix : Propriétaire

        - Linux : Libre

        - Debian : Libre

        - Android : Libre, mais les vendeurs de smartphones rajoutent généralement du code supplémentaire qui lui est propriétaire.

        - Mac OS/Mac OS X: Propriétaire

        - iPhone/iOS : Propriétaire

        - MS-DOS : Propriétaire

        - Windows : Propriétaire

## Commandes linux

Nous utiliserons un système d’exploitation libre : linux.

Comme c’est un logiciel libre, son code a pu être partagé et modifié, il a ainsi été décliné en plusieurs distributions : Debian, Arch Linux, Ubuntu, Linux mint…

Toutes ces distributions étant dérivées de Linux, présentent beaucoup de similarités, notamment au niveau des commandes

!!! faq "Les commandes?"

    Les commandes sont un autre moyen de lancer des programmes, il fut un temps où il n’y avait pas d’interface graphique sur un système d’exploitation, c’était donc le seul moyen de démarrer un programme.

    ![](20221009124059.png)

    Voici à quoi ressemble un système d'exploitation sans interface graphique, cela a un certain charme.

!!! info "Terminal"
    Pour lancer des commandes sur nos systèmes d’exploitation modernes, il faut ouvrir un programme que l’on appelle parfois « terminal », ou « invite de commande » ou « interpréteur de commande »

!!! abstract "Principales commandes"

    - ls : liste les fichiers du répertoire courrant
    - cd dossier: change le répertoire courrant vers «dossier»
    - mkdir d : créer le répertoire d
    - mv f1 f2 : renomme le fichier f1 en f2, permet aussi de déplacer le fichiers
    - cat f : lire le contenu du fichier f
    - chmod  p f: applique les permissions p au fichier f
