# Evaluation 1ere NSI
Nom:

Prénom: 


!!! faq "I/ QCM (6 points)"

    === "Enoncé"
        1\. Parmis ces systèmes d'exploitations, cocher le ou les système(s) libre(s):

        - [ ] macOS
        - [ ] Windows
        - [ ] Linux
        - [ ] Unix

        2\. Quelles sont les fonctions d'un système d'exploitation?

        - [ ] Gestion des fichiers
        - [ ] Gestion de l'interface graphique
        - [ ] Gestion de la mémoire
        - [ ] Gestion des applications

        3\. Je suis dans le répertoire `/home/Utilisateur/`, quelle commande Linux utiliser pour lister les fichiers du répertoire courant?

        - [ ] `ls`
        - [ ] `cd ..`
        - [ ] `help`
        - [ ] `mkdir Utilisateur`

        4\. Soit la ligne suivante en python : `a = 3 + 6`, cocher les affirmations qui sont vraies:

        - [ ] l'expression a prend la valeur `9`
        - [ ] la variable a prend la valeur `9`
        - [ ] `a` est une constante
        - [ ] `3 + 6` est une expression

        5\. Quel est le type de la variable `calcul` après l'exécution de la ligne suivante: `calcul = 3 + 5.0`

        - [ ] Entier (int)
        - [ ] Réel (float)

        6\. Quel est le type de la variable `calcul` après l'exécution de la ligne suivante: `calcul = "3" + 5.0`

        - [ ] Entier (int)
        - [ ] Chaine de caractères (str)
        - [ ] Erreur
        - [ ] Réel (float)

    === "Corrigé"
       1\. Parmis ces systèmes d'exploitations, cocher le ou les système(s) libre(s)

    - ❌ macOS
    - ❌ Windows
    - ✅ Linux
    - ❌ Unix

    2\. Quelles sont les fonctions d'un système d'exploitation?

    - ✅ Gestion des fichiers
    - ❌ Gestion de l'interface graphique
    - ✅ Gestion de la mémoire
    - ✅ Gestion des applications

    3\. Je suis dans le répertoire `/home/Utilisateur/`, quelle commande Linux utiliser pour lister les fichiers du répertoire courant?

    - ✅ `ls`
    - ❌ `cd ..`
    - ❌ `help`
    - ❌ `mkdir Utilisateur`

    4\. Soit la ligne suivante en python : `a = 3 + 6`, cocher les affirmations qui sont vraies:

    - ❌ l'expression a prend la valeur `9`
    - ✅ la variable a prend la valeur `9`
    - ❌ `a` est une constante
    - ✅ `3 + 6` est une expression

    5\. Quel est le type de la variable `calcul` après l'exécution de la ligne suivante: `calcul = 3 + 5.0`

    - ❌ Entier (int)
    - ✅ Réel (float)

    6\. Quel est le type de la variable `calcul` après l'exécution de la ligne suivante: `calcul = "3" + 5.0`

    - ❌ Entier (int)
    - ❌ Chaine de caractères (str)
    - ✅ Erreur
    - ❌ Réel (float)


!!! faq "II/ Représentation des nombres (7 points)"
    === "Enoncé"
        1\. Donner les représentations en décimal (base 10) des nombres binaires suivants : 
        
        `11011`

        ```
         
        ```
        `11110`

        ```

        ```

        `00001`

        ```
        
        ```

        2\. Soit le nombre `421` en décimal (base 10), donner sa représentation en binaire (base 2)

        ```
         
         
         
         
             
         
        ```

        3\. Soit le nombre `301` en décimal (base 10), donner sa représentation en binaire (base 2)

        ```
         
         
         
         
         
         
        ```

    === "Corrigé"
        1\. Donner les représentations en décimal (base 10) des nombres binaires suivants : 
        
        `11011`

        $$2^0\times 1 + 2^1 \times 1 + 2^2\times 0 + 2^3\times 1 + 2^4\times 1 = 1 + 2 + 0 + 8 + 16 = 27$$

        `11110`

        $$2^0\times 0 + 2^1 \times 1 + 2^2\times 1 + 2^3\times 1 + 2^4\times 1 = 0 + 2 + 4 + 8 + 16 = 30$$

        `00001`

        $$2^0\times 1 + 2^1 \times 0 + 2^2\times 0 + 2^3\times 0 + 2^4\times 0 = 1 + 0 + 0 + 0 + 0 = 1$$

        2\. Soit le nombre `421` en décimal (base 10), donner sa représentation en binaire (base 2)

        $$421 / 2 = 210 + 1$$

        $$210 / 2 = 105 + 0$$

        $$105 / 2 = 52 + 1$$

        $$52 / 2 = 26 + 0$$

        $$26 / 2 = 13 + 0$$

        $$13 / 2 = 6 + 1$$

        $$6 /2 = 3 + 0$$

        $$3 / 2 = 1 + 1$$

        $$1 / 2 = 0 + 1$$

        On lit à l'envers et 421 = 110100101

        3\. Soit le nombre `301` en décimal (base 10), donner sa représentation en binaire (base 2)

        $$301 / 2 = 150 + 1$$

        $$150 / 2 = 75 + 0$$

        $$75 / 2 = 37 + 1$$

        $$37 / 2 = 18 + 1$$

        $$18 / 2 = 9 + 0$$

        $$9 / 2 = 4 + 1$$

        $$4 /2 = 2 + 0$$

        $$2 / 2 = 1 + 0$$

        $$1 / 2 = 0 + 1$$

        On lit à l'envers et 301 = 100101101

    4\. Donner la table de vérité de l'expression booléenne suivante : `(non A) ou B`

    | A | B | (non A) | (non A) ou B |
    | - | - | ------- | ------------ |
    | 0 | 0 |     1    |      1        |
    | 0 | 1 |     1    |      1        |
    | 1 | 0 |     0    |      0        |
    | 1 | 1 |     0    |      1        |
    

!!! faq "III/ Programmation (7 points)"

    1\. Donner l'affichage de sortie du programme suivant:

    ```python
    a=102
    b=23
    while a<100 or b<100:
        a = a+b
        b = b*2
        print(a,b)
    ```
    ```
    125 46
    171 92
    263 184
    ```

    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=a%3D102%0Ab%3D23%0Awhile%20a%3C100%20or%20b%3C100%3A%0A%20%20%20%20a%20%3D%20a%2Bb%0A%20%20%20%20b%20%3D%20b*2%0A%20%20%20%20print%28a,b%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>
    

    2\. Donner l'affichage de sortie du programme suivant:
    ```python
    def produits(n):
        s = 1
        for i in range(2*n, 3*n+1):
            s += i
            print(s)
        return s

    produits(2)
    ```

    ```
    5
    10
    16
    ```

    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20produits%28n%29%3A%0A%20%20%20%20s%20%3D%201%0A%20%20%20%20for%20i%20in%20range%282*n,%203*n%2B1%29%3A%0A%20%20%20%20%20%20%20%20s%20%2B%3D%20i%0A%20%20%20%20%20%20%20%20print%28s%29%0A%20%20%20%20return%20s%0A%0Aproduits%282%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    3\. Compléter la fonction suivante qui permet de calculer la somme des inverses des carrés des n premiers entiers non nuls

    ```python 
    def somme-inv(n):
        ___________________________________________
        for i in range(____________________________)
        ___________________________________________
        return ____________________________________
    ```

    ```python
    def somme-inv(1,n):
        s = 0
        for i in range(n):
            s += 1/(n**2)
        return s
    ```
    4\. Recopier et corriger l'algorithme suivant

    ```python
    texte = input("un nombre")
    if texte % 2 == 1:
        print(texte, "est impair")
    else:
        print(texte, "est pair")
    ```

    ```
    Traceback (most recent call last):
    File "/home/Utilisateur/test.py", line 3, in <module>
        if texte % 2 == 1:
    TypeError: not all arguments converted during string formatting
    ```

    Il fallait voir que l'on effectue une opération entre une chaine de caractère et un entier

    ```python
    texte = int(input("un nombre"))
    if texte % 2 == 1:
        print(texte, "est impair")
    else:
        print(texte, "est pair")
    ```

    autre version : 

    ```python
    texte = input("un nombre")
    if texte % 2 == 1:
        print(int(texte), "est impair")
    else:
        print(int(texte), "est pair")
    ```