# Elements HTML/CSS/JS

## Balises HTML

!!! info "Balises HTML"
    
    |Balise|Signification|
    |------|-------------|
    |de `<h1>` à `<h6>`|En-tête de section de niveau 1 à 6|
    |`<p>`|Paragraphe|
    |`<ol>` `<ul>` `<li>`|Liste numérotée (ol) ou à puces (ul, élément de liste (li)|
    |`<b>` `<i>`|Texte en gras ou en italique|
    |`<a>`|Lien vers une autre URL (spécifiée par l'attribut `href`)|
    |`<img>`|Image (URL du contenu spécifiée par l'attribut `src`)|

!!! info "Attributs d'une balise"

    La plupart des éléments acceptent les attributs suivants, qui sont notamment utilisés par les feuilles de style et les formulaires:

    |Attribut|Signification|
    |--------|-------------|
    |`id`|Identifiant unique de l'élément dans le document|
    |`name`|Nom de l'élément (pas nécessairement unique)|
    |`class`|Un ou plusieurs noms de classes pour la feuille de style|

## Propriétés CSS

!!! info "Propriétés CSS"

    |Propriété|Signification|
    |---------|-------------|
    |`color`|Couleur du texte, ex: `red`|
    |`background-color`|Couleur de fond, ex: `gray`|
    |`border`|Epaisseur, style et couleur du bord, ex: `2px solid green`|
    |`width`|Largeur de l'élément, ex: `50%`|
    |`font`|Police de caractères, ex: `italic 12pt sans-serif`|

!!! info "Modifier la propriété CSS d'un élément HTML"
    Modifier la propriété d'un ensemble de balises:

    Exemple le code css suivant modifie la propriété `color` de toutes les balises `<p>`
    ```CSS
    p { color: blue }
    ```

    Modifier la propriété d'une classe d'éléménts:

    Exemple le code css suivant modifie la propriété `border` de tous les éléments de la classe `important`
    ```CSS
    .important { border: 2px solid red}
    ```

    Modifier la propriété d'un élément par son identifiant

    Exemple le code css suivant modifie la propriété `background-color` de l'élément d'identifiant `note`
    ```CSS
    #note { background-color: yellow}
    ```

## Formulaires

Un formulaire est délimité par l'élément HTML `<form>` qui précise l'URL (attribut `action`) et la méthode (attribut `method`) qui seront envoyés
Il contient un ensemble d'éléments interactifs permettant la saisie de données, spécifiés par l'élément `<input>`. Son attribut `type` permet de préciser le type de saisie. Ses principales valeurs sont :

!!! info "Valeurs de l'attribut type de l'élément input"

    - `text` : une zone de texte
    - `number` : pour saisir un nombre
    - `range` : slider pour saisir une valeur numérique
    - `checkbox` : case à cocher
    - `radio` : boutton radio (les cases rondes)
    - `button` : un bouton
    - `hidden` : champ invisible

## Javascript

|Element|Syntaxe|
|-------|-------|
|Nombres|entiers(123) ou réels(123.456)|
|Chaines|`'texte'` ou `"texte"`|
|Booléens|`true` ou `false`|




