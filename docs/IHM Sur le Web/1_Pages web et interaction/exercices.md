# Exercices - pages web statiques

!!! tip "JsFiddle"
    Pour tous les exercices vous pouvez utiliser ce site : [JsFiddle](https://jsfiddle.net/) qui permet d'avoir le code HTML d'un côté, et le code CSS/Javascript de l'autre

## Composants d'une page web

!!! example "Exercice 1"
    Le fragment de code HTML suivant est-il correct? Sinon, comment le corriger?
    ```html
    <ul>
    <li>HTML est le<i>langage du<b>Web</i></b>
    <li><p>C'est un descendant de XML.</li>
    </ul>
    ```

!!! example "Exercice 2"
    a. Ecrire une page HTML qui produise l'affichage ci-contre
    ![](20221011101211.png)

    b. Compléter la page avec une feuille de style CSS pour qu'elle ai l'aspect ci-contre:

    ![](20221011102149.png) 

!!! example "Exercice 3"
    Les propriétés CSS `padding` et `margin` permettent de contrôler l'espace libre autour d'un élément HTML. Leur valeur est soit `0` soit une taille en pixels, par exemple `10px`.

    a. Ecrire une feuille de style pour le code HTML ci-dessous qui donne une marge de 10 pixels et un padding de 0 à l'élément d'identifiant `marge`, et une marge de 9 et un padding de 10 pixels à l'élément d'identifiant `pad`. Y a-t-il une différence entre l'affichage des deux apragraphes?

    ```html
    <p id="marge">Paragraphe avec marge.</p>
    <p id="pad">Paragraphe avec padding. </p>
    ```

    b. Modifier la feuille de style pour donner aux deux paragraphes une couleur de fond (propriété background-color) différente. Que conclure?

    c. Ajouter à la feuille de style un bord à chaque paragraphe. Qu'observe-t-on?

!!! example "Exercice 4"
    a. Ecrire une page HTML contenant un ensemble de paragraphes représentant une conversation. Les paragraphes de la classe CSS moi ou autre selon l'interlocuteur.

    b. Ajouter une feuille de style pour que la présentation de la conversation ressemble à celle d'une application de messagerie instantanée. Que se passe-t-il lorsque l'on change la taille de la fenêtre?

    On pourra utiliser les propriétés CSS suivantes: `margin-left`, `margin-right`, qui contrôlent la taille de la marge gauche et droite d'un élément. `border-radius` qui permet de spécifier le rayon des arrondis des coins d'un élément, et `text-align`, dont la valeur est `left` `right` ou `center`

## Formulaires

!!! example "Exercice 5"

    <form action="https://www.qwant.com/" method="GET">
    <input name="q" type="text">
    <input name="envoyer" type="submit">
    </form>
    
    La requête au moteur de recherche Qwant pour le mot `NSI` est la suivante:
    `https://www.qwant.com/?q=NSI?t=web`

    a. Ecrire et tester un formulaire qui contient un champ de saisie de texte et un bouton et qui effectue une requête auprès de ce moteur de recherche.

    b. Modifier le formulaire pour qu'il effectue une recherche sur le moteur de recherche `https://duckduckgo.com`. Observer l'URL de la réponse à une recherche pour comprendre la forme de la requête.

!!! example "Exercice 6"
    La page HTML [conversion.html](pages/conversion.html) affiche un élément `<input>` de type `range`, c'est à dire une tirette permettant de saisir une valeur numérique.

    !!! info "On doit utiliser les événements"
        Pour utiliser les événements on doit utiliser JavaScript

        Exemple d'un événement dans une balise input:
        ```html
        <input type ="text" onchange="majuscules(this)">
        ```

        ```javascript
        function majuscules(champ){
	        champ.value = champ.value.toUpperCase()
        }
        ```

        La fonction javascript précédente change la valeur de champ en majuscules. La balise input fait appel à l'événement `onchange`, quand le formulaire est modifié la fonction javascript `majuscules` est appelé avec `this` en argument, qui est la valeur du contenu du formulaire.
        
    a. Etudier le code de cette page. A quoi sert l'élément `<span>` ? Quel événement est défini sur l'élément `<input>`? Que fait la fonction de rappel de cet événement?

    b. On veut modifier cette page pour afficher le résultat de la conversion du nombre spécifié par la tirette de degrés centigrades (C) en degrés Fahrenheit (F) : `F = (C - 32) * 5/9`. Ajouter un élément HTML dans lequel s'affiche le résultat de la conversion.

    c. Ajouter un bouton `F<->C` pour inverser le sens de la conversion

!!! example "Exercice 7"
    a. Compléter la page web [calculette.html](pages/calculette.html) pour qu'elle représente une calculette avec une zone de texte pour l'affichage et des boutons pour entrer des chiffres et les 4 operations

    b. Ajouter une fonction de rappel à l'évenement `onclick` à chaque bouton qui appelle l'une des fonctions suivantes : `chiffre(n)` pour les chiffres, `operation(op)` pour les 4 operations, `egale()` et `effacer()` pour les boutons `=` et `C`

    c. Compléter le code javascript pour que la calculette soit fonctionnelle