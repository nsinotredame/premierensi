# Activité échange de messages

## Lancer un serveur web en python

Telecharger le programme [serveur.py](data/serveur.py), qui contient le code d'unj serveur Web écrit en Python et le fichier HTML [conversation.html](pages/conversation.html). 

Lancer le serveur dans un terminal avec la commande `python3 serveur.py` et ouvrir dans un navigateur l'URL [http://127.0.0.1:8000/conversation.html](http://127.0.0.1:8000/conversation.html).

Voilà la page que vous devez obtenir : ![](20221020094923.png)  

!!! faq "A vous!"
    1\. Chercher la signification de l'adresse "127.0.0.1"

    2\. Que se passe-t'il si l'on tape un message et que l'on clique sur envoyer?

    Pour résoudre ce problème il nous faut une page dynamique

## Créer une page dynamique

Les pages d'un service Web, par exemple un site de e-commerce, ne sont pas des documents statiques stockés dans des fichiers que le serveur peut simplement envoyer au client (le navigateur Web). Elles sont calculées par le serveur Web, en général à partir de patrons ("templates" en anglais) de pages HTML, de données qui sont en général stockées dans une base de données et des parametres de la requête. Le langage PHP est souvent utilisé à cet effet. Dans cette activité, nous utilisons à la place la bibliothèque **jinja2** .

Ici nous voulons que la page affiche le contenu de la conversation. Celle-ci est représentée dans le serveur par une liste de chaines de caractères, par exemple `['Hello','Bonjour']`. Le serveur combine le patron avec cette liste pour produire le contenu HTML envoyé au navigateur. On voit dans le patron une boucle `for` qui référence la variable `messages` et donc le corps est un élément de liste HTML qui affiche le message. 

Patron:

```
<ul>
    { % for msg in messages %}
    <li>{{msg}}</li>
    { % endfor %}
</ul>
```

Résultat:

```HTML
<ul>
    <li> Hello </li>
    <li> Bonjour </li>
</ul>
```

Le code qui applique le patron est le suivant :

```python
from serveur import get_template, render
conversation = ['Hello', 'Bonjour']             # La conversation à afficher
template  = get_template('conversation.html')   # Charger le patron
vars = {'messages' : conversation}              # Définir les variables du patron
html = render(template, vars)                   # Appliquer le patron
```
!!! example "A vous!"
    Ajouter le patron ci-dessus dans le fichier `conversation.html`, juste avant la balise de fin `</body>`. Télécharger le fichier [dynserveur.py](data/dynserveur.py).
    Arrêter le serveur lancé précédemment en tapant ++ctrl+"C"++, puis lancer le nouveau serveur `python3 dynserveur.py` et ouvrir la page [127.0.0.1:8000/conversation.html](http://127.0.0.1:8000/conversation.html)

    1\. Qu'affiche la page? Modifier les messages de la conversation dans `dynserveur.py`, relancer le serveur et recharger la page pour voir le nouveau résultat.

    Examiner le code et les commentaires de `dynserveur.py`. La fonction `pageDynamique` associe le nom de page Web `"/conversation.html"` à une fonction, `page_conversation`. Cette fonction sera appelée lorsque le serveur recevra une requête pour cette page. La fonction `page_conversation` charge le patron `conversation.html`, lui applique le contenu de la conversation et retourne la page HTML résultante.

## Répondre à un formulaire

Examiner le code HTML du fichier `conversation.html` et identifier la balise `<form>`. Il s'agit du formulaire de saisie de message. La balise `<form>` contient un attribut `action` dont la valeur est l'URL `/message` qui sera transmise au serveur. Le formulaire contient les balises `<input>` correspondant au champ de saisie de texte, de nom `msg` et au bouton d'envoi. Lorsque l'on clique le bouton `Envoyer`, le navigateur demande au serveur la page `/message` et lui communique les valeurs saisies dans le formulaire, ici le contenu du message.

Le serveur répond à cette requête en récupérant le texte du message et en renvoyant le contenu de la page dynamique `/conversation.html`, qui va afficher la nouvelle conversation. 

!!! example "A vous!"
    Télécharger le fichier [nouveau_message.py](data/nouveau_message.py) et insérer son contenu dans le fichier `dynserveur.py` (avant `lancerServeur()`). relancer le serveur et recharger la page.

    1\. Que se passe-t-il maintenant si l'on tape un message et que l'on clique sur `Envoyer` ? Ouvrir plusieurs onglets du navigateur sur la même page. Que se passe-t'il si l'on envoie des messages depuis différents onglets?

    2\. Modifier la ligne 6 du code ci-dessus et le fichier `conversation.html` pour lister les messages dans l'ordre chronologique et mettre le formulaire à la fin

    3\. Comment communiquer avec vos voisins via ce chat?