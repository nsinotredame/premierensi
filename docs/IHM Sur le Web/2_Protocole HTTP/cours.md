# 2 Protocole HTTP

La communication entre clients et serveurs Web utilise le protocole HTTP ("Hypertext Transfer Protocol") ou sa version sécurisée HTTPS. Une **requête** HTTP est constituée d'une **méthode** qui indique l'opération souhaitée, de l'URL de la ressource à laquelle on applique l'opération, d'un ensemble d'**en-têtes** qui fournissent des informations complémentaires au serveur, par exemple le type de navigateur, et d'un **contenu** éventuel. Les principales méthodes sont : 

- **GET** pour récupérer le contenu de la ressource spécifiée par l'URL
- **POST** pour modifier la ressource spécifiée par l'URL
- **PUT** pour créer ou remplacer le contenu de la ressource spécifiée par l'URL
- **DELETE** pour détruire la ressource spécifiée par l'URL

!!! example "Identifier les méthodes HTTP"
    1\. Rendez vous sur le lien [fr.wikipedia.org](https://fr.wikipedia.org), appuyer sur la touche F12 sur votre navigateur, rendez vous sur la section "Réseau", recharger la page, notez les requêtes envoyées et leurs méthode

    2\. Rendez vous sur école directe, si vous êtes connecté, déconnectez vous, appuyer sur F12, rendez vous sur la section "Réseau" puis connectez vous à votre compte école directe, notez les méthodes des requêtes envoyées.

    3\. Recherchez : A quoi correspond le code "200" en HTTP? A quoi correspond le code "404"? Et le code "418"?

