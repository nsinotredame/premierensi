from serveur import get_template, render, OK, Redirect, pageDynamique, lancerServeur

# Les messages de la conversation
conversation = ['Hello', 'Bonjour']

# La page dynamique qui affiche une conversation
def page_conversation(url, vars):
	""" Retourner la page de la conversation """
	# charger le patron
	template = get_template('conversation.html')
	# définir les variables
	vars['messages'] = conversation
	# appliquer le patron
	html = render(template, vars)
	# retourner la page au navigateur
	return OK(html)

# Définir la page dynamique :
# Le serveur appelera la fonction `page_conversation` 
# lorsqu'il recevra un requête pour la page `/conversation.html`.
pageDynamique('/conversation.html', page_conversation)

# Lancer le serveur
lancerServeur()
