
# Insérer ce code dans le fichier dynserveur.py, avant `lancerServeur()`.

# La page dynamique qui ajoute un message
def nouveau_message(url, vars):
	""" Ajouter un message à la conversation et afficher celle-ci """
	# récuperer le texte du message envoyé par le navigateur
	message = vars['msg']
	# l'insérer au début de la conversation
	conversation.insert(0, message)
	# renvoyer la page conversation.html, ce qui aura pour effet
	# d'appeler `page_conversation` pour afficher la nouvelle liste de messages
	return Redirect('/conversation.html')

# Définir la page dynamique
pageDynamique('/message', nouveau_message)  # associer la page dynamique
