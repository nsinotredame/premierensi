# Projets
## Puissance 4

Tout le monde connait le puissance 4? A vous de le programmer maintenant, vous avez toutes les clés pour le faire:

- structures de contrôle (if/else)
- fonctions
- boucles
- tableaux à deux dimensions

L'objectif sera dans un premier temps de réaliser le jeu sans interface graphique, donc avec des `print`. Si vous êtes motivé et que vous savez comment faire, vous pouvez bien entendu faire une version avec interface graphique.

!!! info "Regles du jeu" 
    Le jeu de puissance 4 se joue à deux joueurs avec une grille de 7 colonnes et 6 rangées. Un joueur possède des jetons rouges et l'autre des jetons jaunes. Les joueurs mettent chacun leur tour un jeton dans une colonne. Le but est d'être le premier à aligner 4 jetons verticalement, horizontalement ou en diagonale. Si personne n'arrive à aligner 4 jetons avant que la grille ne soit remplie il y a match nul. 


### Travail demandé

C'est un projet complexe que l'on peut décomposer en différentes étapes correspondant à des fonctions Python réalisant chacune une action. Nous allons détailler ces étapes ici. La grille du jeu sera représenté par un tableau à deux dimensions. Les 0 correspondent à des cases vides, les 1 aux jetons du joueur 1 et les 2 aux jetons du joueur 2.


!!! info "Fonctions à réaliser"
    
    - `grille_init() `: fontion qui renvoie un tableau de 6 lignes et 7 colonnes remplies de zéros ;

    - `affiche_grille(tab)` : fonction qui affiche la grille du jeu dans la console de la façon la plus esthétique possible (voir la suggestion ci-dessous à améliorer) ;

    ```
        +---+---+---+---+---+---+---+
        | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
        | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
        | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
        | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
        | 0 | 0 | 2 | 1 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
        | 0 | 0 | 1 | 2 | 0 | 0 | 0 |
        +---+---+---+---+---+---+---+
    ```

    - `colonne_libre(tab, colonne)` : fonction qui renvoie un booléen indiquant s'il est possible de mettre un jeton dans la colonne (indique si la colonne n'est pas pleine) ;
    
    - `place_jeton(tab, colonne, joueur)` : fonction qui place un jeton du joueur (1 ou 2) dans la colonne. Elle renvoie la grille modifiée ;
    
    - `horizontale(tab, joueur)` : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une ligne ;
    
    - `verticale(tab, joueur)` : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une colonne ;
    
    - `diagonale(tab, joueur)` : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une diagonale ;
    
    - `gagne(tab, joueur)` : fonction qui renvoie True si le joueur a gagné ;
    
    - `tour_joueur(tab, joueur)` : fonction qui permet au joueur de placer un jeton dans la colonne choisie. Elle indique si la colonne est pleine et permet alors au joueur de choisir une autre colonne ;
    
    - `egalite(tab)` : fonction qui renvoie True s'il y a égalité et False sinon ;
    
    - `jouer(tab)` : fonction qui permet aux deux joueurs de jouer chacun leur tour. Elle vérifie que les joueurs n'ont pas gagné à la fin de leur tour. Si l'un des deux à gagné ou s'il y a égalité, elle donne le résultat ;

!!! warning "Propreté du code"
    Nous verrons dans ce projet et au cours de l'année, l'importante des **commentaires**.

    Les commentaires sont des sections de code qui ne sont pas exécutées par python, elles servent à donner des informations à la personne (humaine) qui va lire le programme ensuite.

    C'est très **important** notemment pour le travail en équipe, afin que votre code soit lisible par une autre personne, de la même manière que vous soignez votre orthographe et votre écriture manuscrite.

    Même pour vous c'est important afin de reprendre votre code plus tard.

    ![](20221004125823.png){: .center width=30%}

    La propreté du code ne dépend pas que des commentaires, les noms de variables doivent être explicites, par exemple une variable servant à stocker un maximum doit s'appeller `maximum` et non `a`

!!! warning "Plagiat" 
    Je sais que vous allez trouver du code sur internet, d'autres personnes ont même déjà surement implémenté ce projet en python.

    Le plagiat n'existe pas vraiment en informatique tant que du code est open source, néanmoins pour ce projet, je veux **VOTRE** version du code.

    Si vous copiez/collez du code de quelque part sur internet, google existe, ça se retrouve. Vous avez bien sur le droit de vous inspirer, mais il faut alors savoir expliquer ce que vous avez pris.

    Ce que je ne veux pas voir : 

    ![](20221004131627.png){: .center width=30%}

!!! tip "Tests"
    Voici quelques tests, qui devraient passer si vous avez correctement implémenté vos fonctions
    ```python
    tab = grille_init()
    place_jeton(tab, 0, 1)
    place_jeton(tab, 1, 1)
    place_jeton(tab, 2, 1)
    place_jeton(tab, 3, 1)
    assert(horizontale(tab, 1))
    ```

    ```python
    tab = grille_init()
    place_jeton(tab, 1, 1)
    place_jeton(tab, 1, 1)
    place_jeton(tab, 1, 1)
    place_jeton(tab, 1, 1)
    place_jeton(tab, 1, 1)
    place_jeton(tab, 1, 1)
    assert(verticale(tab, 1))
    assert(not(colonne_libre(tab, 1))
    ```


## Site de chat

### Objectif
!!! info "Objectif"
    Vous allez réaliser un site de chat avec le framework python flask, qui permet de réaliser un serveur web dynamique

    Votre site sera constitué d'une unique page (page d'accueuil) contenant un formulaire pour envoyer un message, ainsi que de tous les messages précédemments envoyés

    Votre page sera stylisé avec du css (couleurs, bordures, etc...)

    Afin que tout le monde réalise quelque chose à la hauteur de son ambition, vous choisirez votre niveau de difficulté, vous serez évalués de la même manière peu importe le niveau de difficulté choisi.

    !!! example "Niveaux de difficulté"

        - Niveau 1 : Les messages sont envoyés anonymement

        - Niveau 2 : Les messages sont accompagnés du nom de l’auteur qui le spécifie au moment de l’envoi du message

        - Niveau 3 : Les messages sont accompagnés du nom de l’auteur + de la date et de l’heure

        - Niveau 4 : Les messages sont stockés dans un fichier et rechargés au démarrage du serveur

### Consignes
!!! info "Consignes"
    Vous devez **impérativement** partir de cette base de code : [Forum_Flask](Forum_Flask.zip)

    !!! tip "Pour bien travailler"

        - Telecharger l'archive
        ![](20230302083748.png)

        ![](20230302084547.png)

        - Extraire l'archive dans l'emplacement de votre choix

        ![](20230302084719.png)

        - Ouvrir le dossier avec VSCodium

        ![](20230302084818.png)

        ![](20230302084845.png)

        - Vous pouvez modifier le fichier python flask ainsi que les templates HTML

        ![](20230302091834.png)

    Vous rendrez ce même dossier avec vos ajouts et modifications, ainsi que d'un **document texte** contenant les réponses aux questions suivantes:

    !!! question "Questions:"

        - 1 : A quoi servent les templates dans python flask? **(1 point)**

        - 2 : Que font ces lignes dans le fichier `index.html` ? **(2 points)**

        ![](20230302093229.png)

        - 3 : A quoi servent les attributs `action` et `method` de la balise `<form>` en HTML? **(1 point)**

        - 4 : A quoi sert la balise `<div>` en HTML? **(1 point)**

        - 5 : Comment récupérer les données d'une requête POST sur python flask? **(1 point)**

        - 6 : A quoi sert le paramètre `messages=messages` dans l'appel de fonction `render_template` ? **(1 point)**

        - 7 : Quelle est la fonction de la ligne `<link rel="stylesheet" href="/static/css/style.css">` dans le fichier `base.html` ? **(1 point)**

### Aides
!!! tip "Aides"
    La plupart des informations nécessaires pour répondre aux questions et pour réaliser le projet sont dans le TP sur flask précédemment réalisé : [TP_Flask](Tp_Flask.zip) (mêmes consignes pour l'ouvrir que précédemment)

    Voici des liens qui pourraient vous être utiles:

    [Documentation Flask](https://flask.palletsprojects.com/en/2.1.x/)

    [Documentation Jinja (templates)](https://jinja.palletsprojects.com/en/3.1.x/templates/)

    [Aide utilisation de templates en français](https://openclassrooms.com/fr/courses/4425066-concevez-un-site-avec-flask/4526123-decoupez-votre-projet-en-templates)

    [Aide HTML](https://fr.w3docs.com/apprendre-html/introduction-au-html.html)

    [Documentation HTML](https://fr.w3docs.com/apprendre-html/tableau-des-tags-html.html)

    [Jeu flexbox froggy pour apprendre à utiliser la propriété flex en css](https://flexboxfroggy.com/#fr)

### Barème
!!! info "Barème"

    - Questions : 8 points

    - Code : 12 points dont:
    
        - HTML : 
            - Formulaire : 3 points
            - Messages : 3 points
        
        - Mise en forme CSS : 3 points

        - Python flask : 
            - Enregistrement des messages: 3 points
    

