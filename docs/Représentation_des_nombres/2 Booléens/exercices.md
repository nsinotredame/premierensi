!!! example "Exercice 1"
    === "Enoncé" 
        On se rappelle de l'exercice où il fallait déterminer si une année est bissextile ou non.

        Nous avions fait ce programme avec plusieurs conditionnelles, essayez de refaire ce programme en utilisant une simple expression booléenne, et donc en utilisant une seule conditionnelle.

        {{IDE()}}

    === "Aide 1"
        Définissez d'abord votre expression booléenne.

        Une année est bissextile si:
        
        - Elle est divisible par 4 sans être divisible par 100
        
        ou 

        - Elle est divisible par 400
    === "Aide 2"
        L'expression `n % m == 0` permet de savoir si un nombre `n` est divisible par `m` en python

!!! example "Exercice 2"
    === "Enoncé"
        ```python
        not (a and b) == not a or not b
        not (a or b) == not a and not b
        ```
        Montrer les identités suivantes, appelées identités de l'algèbre de Boole en écrivant et en exécutant un programme qui calcule les valeurs des 4 expressions ci-dessus pour toutes les valeurs possibles des variables booléennes `a` et `b`

        Donner ensuite la table de vérité de chaque expression

        {{IDE()}}

    === "Aide"
        Pour la table de vérité, décomposer chaque expression en expression plus simple

!!! example "Exercice 3"
    === "Enoncé"
        Simplifier l'expression suivante:
        ```python
        (not (b1 < b2)) and (not (b1 > b2))
        ```

!!! example "Exercice 4"
    === "Enoncé"
        L'opérateur de logique binaire "implication", qui s'écrit ⇒ et se lit "A implique B", est défini comme suit:
        `A ⇒ B = (non A) ou B`

        - Donner la table de vérité de cet opérateur

        - Expliquer pourquoi il s'appelle "implication"

        - Donner la table de vérité de l'expression `(A ⇒ B) et (B ⇒ A)`, pourquoi cet opérateur s'appelle-t-il "équivalence"?
