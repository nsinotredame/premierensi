# Cours de Première NSI

Site des cours de première NSI au lycée Notre Dame à Rezé

Pour faire du python chez vous:

!!! tip "Environnement dans le navigateur" 
    Vous pouvez utiliser cet environnement python dans votre navigateur au cas où vous n'avez pas de quoi faire du python à la maison
    {{IDE()}}

!!! tip "Try jupyter si vous avez un code conséquent à tester"
    [Try jupyter](https://jupyter.org/try) (sélectionner jupyter notebook, ensuite file -> new -> notebook)

!!! tip "Google colab si vous avez un compte google"
    [Google colab](https://colab.research.google.com/)


Vidéo intéressante sur chatgpt : 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/R2fjRbc9Sa0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

